// Inventory Module All Jquery / Javasript
var Inventory = (function () {
    var options = {};
    var inventory, gridId, makes;
    var $table, $modal, $form;

    /**
     * Setup module vars
     */
    var setup = function () {
        gridId = options.table;
        $table = $(options.table);
        $modal = $(options.modal);
        $form = $(options.form);
        getInventory();
        getModels();
    };

    /**
     * Setup Default event triggers
     */
    var setupEvents = function () {
        $(".add").on("click", function () {
            addRow();
        });
        $(".save").on("click", function () {
            saveRow();
        });
        $('.cancel').on('click', function () {
            cancel();
        });
        $modal.on('hidden.bs.modal', function () {
            cancel();
        })
        $(':input', $form).on("change", function(){
            validate();
        });
    };

    /**
     * Handles cancel event
     */
    var cancel = function(){
        var isNew = $form.data('new');
        if(isNew) {
            var id = $("#id").val();
            deleteRow(id);
        }
    };

    /**
     * Setup row event triggers
     * @param context
     */
    var setupRowEvents = function (context) {
        var context = context || $table;

        $(".edit", context).on("click", function () {
            var id = $(this).data("id");
            editRow(id);
        });
        $(".delete", context).on("click", function () {
            var id = $(this).data("id");
            deleteRow(id);
        });
    };

    /**
     * Fetch the current inventor from external source
     */
    var getInventory = function () {
        $.ajax(options.url, {
            success: function (resp) {
                inventory = resp.data;
                updateTable();
            }
        });
    };

    /**
     * Fetch the current model data from external source
     */
    var getModels = function () {
        $.ajax(options.makes, {
            success: function (resp) {
                makes = resp;
                setupForm();
            }
        });
    };

    /**
     * Check if form is valid
     */
    var isValid = function(){
        return validate();
    };

    /**
     * Validate the form.... manual instead of parsely.js or something similar
     * @returns {boolean}
     */
    var validate = function(){
        var valid = true;
        $(':input', $form).each(function () {
            if(!valid){
                return valid;
            }
            var $element = $(this);
            var elementId = $element.attr('id');
            var value = $element.val();

            switch (elementId) {
                case 'price':
                    valid = (value >= 0) && (!isNaN(parseFloat(value)));
                    if(!valid){
                        $element.addClass("has-error").focus();
                    }else{
                        $element.removeClass("has-error");
                    }
                    break;
                case 'inventory':
                    valid = (value >= 0) && (!isNaN(parseInt(value)));
                    if(!valid){
                        $element.addClass("has-error").focus();
                    }else{
                        $element.removeClass("has-error");
                    }
                    break;
                default:
                    $element.removeClass("has-error");
                    break;
            }
        });

        return valid;
    }

    /**
     * Initiate the form values and add the event triggers
     */
    var setupForm = function () {
        $.each(makes, function (index, make) {
            var $op = $('<option value="' + make.value + '">' + make.title + '</option>');
            $('#make').append($op);
        });

        $('#make').on('change', function () {
            updateForm();
        });

        $('.spinner .btn:first-of-type').on('click', function() {
            var val = parseInt($('.spinner input').val(), 10);
            $('.spinner input').val( val + 1);
        });
        $('.spinner .btn:last-of-type').on('click', function() {
            var val = parseInt($('.spinner input').val(), 10);
            if(val > 0) {
                $('.spinner input').val(val - 1);
            }
        });
    };

    /**
     * Update the form on item change
     * @param item
     */
    var updateForm = function (item) {
        if (item != null) {
            $.each(item, function (index, value) {
                $('#' + index).val(value);
            });
        }

        var currentMake = $("#make").val();
        $('#model').empty();
        $.each(makes, function (index, make) {
            if (currentMake == make.value) {
                $.each(make.models, function (index2, model) {
                    var $op = $('<option value="' + model.value + '">' + model.title + '</option>');
                    $('#model').append($op);
                });
                if (item != null) {
                    $('#model').val(item.model)
                }
            }
        });
    };

    /**
     * Update the table after form has been saved
     */
    var updateTable = function () {
        $.each(inventory, function (index, value) {
            var rowId = "#row_" + value.id;
            row(index, value, rowId);
            setupRowEvents(rowId);
        });
    }

    /**
     * Add or edit html for an existing row in the table
     * @param index
     * @param data
     * @param rowId
     */
    var row = function (index, data, rowId) {
        if ($(rowId, $table).length > 0) {
            $.each(data, function (index, value) {
                if(index == "price") {
                    value = numberFormat(value, '$ ');
                };
                $('.' + index, $(rowId)).html(value);
            });
        } else {
            var html = "";
            $.each(data, function (index, value) {
                if(index == "price") {
                    value = numberFormat(value, '$ ');
                }
                html = html + '<td class="' + index + '">' + value + '</td>';
            });
            html = html + '<td><button data-id="' + data.id + '" class="edit btn btn-primary btn-sm">Edit</button>'
                + '&nbsp;&nbsp;&nbsp;'
                + '<button data-id="' + data.id + '" class="delete btn btn-primary btn-sm">Delete</button></td>';
            $('tbody', $table).append('<tr id="row_' + data.id + '">' + html + '</td>');
        }
    }

    /**
     * Format a number as price or something else
     * @param value
     * @param prefix
     * @returns {string}
     */
    var numberFormat = function (value, prefix){
        var pref = prefix || '';
        return pref + parseFloat(value).toFixed(2);
    };

    /**
     * Save info from the form to the row
     */
    var saveRow = function () {
        if(isValid()) {
            $form.data('new', false);
            var key = findIndex($("#id").val());
            $form.find(':input').each(function () {
                var $element = $(this);
                $.each(inventory[key], function (index, value) {
                    var elementId = $element.attr('id');
                    var value = $element.val();
                    switch (elementId) {
                        case 'price':
                            inventory[key][elementId] = parseFloat(value).toFixed(2);
                            break;
                        case 'inventory':
                            inventory[key][elementId] = parseInt(value, 10);
                            break;
                        default:
                            inventory[key][elementId] = value;
                            break;
                    }
                });
            });
            $modal.modal('hide');
            updateTable();
        }else{
            alert("Please enter valid data");
        }
    };

    /**
     * Edit the row
     * @param id
     */
    var editRow = function (id) {
        var index = findIndex(id);
        var inv = inventory[index];
        updateForm(inv);
        $modal.modal('show');
    };

    /**
     * Delete a row
     * @param id
     */
    var deleteRow = function (id) {
        var index = findIndex(id);
        if (index > -1) {
            inventory.splice(index, 1);
            $("#row_" + id).remove();
        }
    };

    /**
     * Create a new row, auto generate an ID
     */
    var addRow = function () {
        $form.data('new', true);
        var newRow = {
            "id": makeId(),
            "make": '',
            "model": '',
            "price": 0,
            "inventory": 0,
        };
        inventory.push(newRow);
        updateTable();
        editRow(newRow.id);
    };

    /**
     * Id Generator
     * @returns {string}
     */
    var makeId = function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    };

    /**
     * Find the array index of the row based on the id
     * @param id
     * @returns {*}
     */
    var findIndex = function (id) {
        var idx = null;
        $.each(inventory, function (index, value) {
            if (value.id == id) {
                idx = index;
            }
        });

        return idx;
    };

    return {
        init: function (op) {
            options = $.extend({
                'table': '#inventoryTable',
                'url': '/public/inventory.json',
                'makes': '/public/models.json',
                'modal': '#myModal',
                'form': '#inventoryForm'
            }, op);

            setup();
            setupEvents();
        }
    }
})(jQuery);